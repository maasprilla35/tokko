<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">google.load('jquery', '1.7.1');</script>
<script type="text/javascript">google.load('jqueryui', '1.8.13');</script>
</head>

<body>
<?php 
include "api.inc";
$auth = new TokkoAuth('978f209b114a8558bd19269707dc98ac4f9ea3fc');
$search_form = new TokkoSearchForm($auth);
?>

<link rel="stylesheet" href="estilos_buscador.css">
<div id="buscador_prop">
     
          <div>
            <?php 
                $countries2 = new TokkoCountries();
                $countries2->deploy_select_box('countries_select_box', 'countries_select_box', '', '1', 'Seleccione un pais');
                $states = new TokkoStates();
                $states->deploy_select_box('states_select_box', 'states_select_box', '', '', 'Seleccione una provincia');
                $states->connect($countries2);
                $states->ajax_deploy();
                $divisions = new TokkoDivisions();
                $divisions->deploy_select_box('divisions_select_box', 'divisions_select_box', '', '', 'Seleccione un barrio');
                $divisions->connect($states);
                $divisions->ajax_deploy();

             ?>
          </div>
    
</div>

</body>
