<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Apartment - Premium Real Estate HMTL Site Template</title>
	<meta name="keywords" content="Download, Apartment, Premium, Real Estate, HMTL, Site Template, property, mortgage, CSS" />
	<meta name="description" content="Download Apartment - Premium Real Estate HMTL Site Template" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<!-- Bootstrap -->
	<link rel="stylesheet" href="bootstrap/bootstrap.min.css">
	<!-- Font awesome styles -->
	<link rel="stylesheet" href="apartment-font/css/font-awesome.min.css">
	<!-- Custom styles -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,500italic,700,700italic&amp;subset=latin,latin-ext'>
	<link rel="stylesheet" type="text/css" href="css/plugins.css">
	<link rel="stylesheet" type="text/css" href="css/apartment-layout.css">
	<link id="skin" rel="stylesheet" type="text/css" href="css/apartment-colors-blue.css">

	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- jQuery  -->
	<script type="text/javascript" src="js/jQuery/jquery.min.js"></script>
	<script type="text/javascript" src="js/jQuery/jquery-ui.min.js"></script>

</head>

<body>
		<?php
		include "api.php";
		$auth = new TokkoAuth('978f209b114a8558bd19269707dc98ac4f9ea3fc');
		$search_form = new TokkoSearchForm($auth);
		?>
	<!-- <div class="loader-bg"></div> -->
	<div id="wrapper" class="wrapper2">


		<!-- Page header -->
		<header class="header2" style="background-color: white; border-bottom: 3px solid rgb(251, 175, 51);">
			<div class="top-bar-wrapper">
				<div class="container top-bar">
					<div class="row">
						<div class="col-xs-6 col-sm-12">
							<div class="top-mail pull-right hidden-xs">
								<span class="top-icon-circle pull-rich">
									<i class="fa fa-envelope fa-sm"></i>
								</span>
								<span class="top-bar-text">info@barrerapropiedades.com.ar</span>
							</div>
							<div class="top-phone pull-right hidden-xxs">
								<span class="top-icon-circle pull-rich">
									<i class="fa fa-phone"></i>
								</span>
								<span class="top-bar-text">02234865544</span>
							</div>
							<div class="top-phone pull-right hidden-xxs">
								<span class="top-icon-circle pull-rich">
									<i class="fa fa-map-marker"></i>
								</span>
								<span class="top-bar-text"> Matheu 163, Mar del Plata.</span>
							</div>
						</div>
						<div class="col-xs-7 col-sm-4">
							<div class="top-social-last top-dark pull-right" data-toggle="tooltip" data-placement="bottom" title="Login/Register">
								<a class="top-icon-circle" href="#login-modal" data-toggle="modal">
									<i class="fa fa-lock"></i>
								</a>
							</div>


						</div>
					</div>
				</div>
				<!-- /.top-bar -->
			</div>
			<!-- /.Page top-bar-wrapper -->
			<nav class="navbar main-menu-cont">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						 aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar icon-bar1"></span>
							<span class="icon-bar icon-bar2"></span>
							<span class="icon-bar icon-bar3"></span>
						</button>
						<a href="index.html" title="" class="navbar-brand">
							<img src="images/logo.png" style="height: 50px;" alt="Apartment - Premium Real Estate Template" />
						</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="index.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"
								 style="color: black;">Alquiler</a>

							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: black;">Alquiler temporada</a>

							</li>

							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: black;">Nosotros</a>

							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: black;">Tasación</a>

							</li>
							<li class="dropdown">
								<a href="contact2.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"
								 style="color: black;">Contacto</a>

							</li>
							<li>
								<a href="submit-property.html" class="special-color">Emprendimientos</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- /.mani-menu-cont -->
		</header>

		<section class="no-padding adv-search-section">
			<!-- Slider main container -->
			<div id="swiper2" class="swiper-container">
				<div class="container swiper2-navigation">
					<div class="row">
						<div class="col-xs-2">
							<a href="#" class="navigation-box2 navigation-box-prev slide-prev">
								<div class="navigation-triangle"></div>
								<div class="navigation-box-icon2">
									<i class="jfont">&#xe800;</i>
								</div>
							</a>
						</div>
						<div class="col-xs-2 col-xs-offset-8">
							<a href="#" class="navigation-box2 navigation-box-next slide-next">
								<div class="navigation-triangle"></div>
								<div class="navigation-box-icon2">
									<i class="jfont">&#xe802;</i>
								</div>
							</a>
						</div>
					</div>
				</div>
				<!-- Additional required wrapper -->
				<div class="swiper-wrapper">
					<!-- Slides -->
					<div class="swiper-slide swiper-lazy" data-background="images/slides/1.jpg">
						<div class="container">
							<div class="row">
								<div class="col-xs-8 col-xs-offset-2 animated fadeInDown slide2-desc slide2-desc-1">
									<h1 class="second-color">Fort Collins, Colorado 80523
										<span class="special-color">.</span>
									</h1>
									<div class="clearfix"></div>
									<p class="swiper2-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
										aliqua.
									</p>
									<div class="swiper2-buttons margin-top-45">
										<div class="swiper2-price-button">$320 000</div>

										<a href="estate-details-right-sidebar.html" class="button-primary">
											<span>read more</span>
											<div class="button-triangle"></div>
											<div class="button-triangle2"></div>
											<div class="button-icon">
												<i class="fa fa-search"></i>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="slider-overlay"></div>
					</div>
					<div class="swiper-slide swiper-lazy" data-background="images/slides/2.jpg">
						<div class="container">
							<div class="row">
								<div class="col-xs-8 col-xs-offset-2 animated slide2-desc slide2-desc-2">
									<h1 class="second-color">West Fourth Street, New York 10003
										<span class="special-color">.</span>
									</h1>
									<div class="clearfix"></div>
									<p class="swiper2-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
										aliqua.
									</p>
									<div class="swiper2-buttons margin-top-45">
										<div class="swiper2-price-button">$550 000</div>

										<a href="estate-details-right-sidebar.html" class="button-primary">
											<span>read more</span>
											<div class="button-triangle"></div>
											<div class="button-triangle2"></div>
											<div class="button-icon">
												<i class="fa fa-search"></i>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="slider-overlay"></div>
						<div class="video-slide">
							<video poster="images/slides/2.jpg" preload="auto" loop autoplay muted>
								<source src='images/slides/2.mp4' type='video/mp4' />
							</video>
						</div>
					</div>
					<div class="swiper-slide swiper-lazy" data-background="images/slides/3.jpg">
						<div class="container">
							<div class="row">
								<div class="col-xs-8 col-xs-offset-2 animated slide2-desc slide2-desc-3">
									<h1 class="second-color">E. Elwood St. Phoenix, AZ 85034
										<span class="special-color">.</span>
									</h1>
									<div class="clearfix"></div>
									<p class="swiper2-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
										aliqua.
									</p>
									<div class="swiper2-buttons margin-top-45">
										<div class="swiper2-price-button">$395 000</div>

										<a href="estate-details-right-sidebar.html" class="button-primary">
											<span>read more</span>
											<div class="button-triangle"></div>
											<div class="button-triangle2"></div>
											<div class="button-icon">
												<i class="fa fa-search"></i>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="slider-overlay"></div>
					</div>
				</div>
			</div>

			<form class="simple-search-form" action="#">
				<div class="container">
					<div class="row">
						<div class="col-xs-9">
							<div class="simple-search-cont" style="margin-left: 13%;">
								<select name="transaction1" class="bootstrap-select" title="Transacción:" multiple>
									<option>Vender</option>
									<option>Alquilar</option>
								</select>

<div id="content">
  <div id="searchfield">
 <form><input type="text" name="currency" class="biginput" id="autocomplete"></form>
  </div><!-- @end #searchfield -->
  
  <div id="outputbox">
 <p id="outputcontent"></p>
  </div>
</div><!-- @end #content -->
								<!-- <input type="text" name="simple-search" class="simple-search-input" placeholder="... Ingresar ubicación, Barrio o palabra clave"
								/> -->
								<a onclick="buscarPropiedades()" class="button-primary pull-right">
									<span>Buscar propiedad</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon">
										<i class="fa fa-search"></i>
									</div>
								</a>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</section>

		<section class="section-light bottom-padding-45 section-both-shadow">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-lg-3">
						<div class="feature wow fadeInLeft" id="feature1">
							<div class="feature-icon center-block">
								<i class="fa fa-building"></i>
							</div>
							<div class="feature-text">
								<h5 class="subtitle-margin">Barrera propiedades</h5>
								<h3>TRAYECTORIA
									<span class="special-color">.</span>
								</h3>
								<div class="title-separator center-block feature-separator"></div>
								<p>Más de 60 años en el mercado inmobiliario. Somos Brokers especializados en el sector y aplicamos toda nuestra experiencia
									para llevar a cabo operaciones más seguras y convenientes.</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="feature wow fadeInUp" id="feature2">
							<div class="feature-icon center-block">
								<i class="fa fa-home"></i>
							</div>
							<div class="feature-text">
								<h5 class="subtitle-margin">ATENCIÓN PERSONALIZADA</h5>
								<h3>BUENOS AIRES MAR DEL PLATA
									<span class="special-color">.</span>
								</h3>
								<div class="title-separator center-block feature-separator"></div>
								<p>Dentro de nuestra gestión brindamos asesoramiento tanto en arquitectura y diseño para los distintos usos comerciales
									del inmueble para su mayor beneficio.</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="feature wow fadeInUp" id="feature3">
							<div class="feature-icon center-block">
								<i class="fa fa-industry"></i>
							</div>
							<div class="feature-text">
								<h5 class="subtitle-margin">Asesoramiento online</h5>
								<h3>QUIERO VENDER
									<span class="special-color">.</span>
								</h3>
								<div class="title-separator center-block feature-separator"></div>
								<p>Nuestra amplia y diversificada cartera de propiedades, la dinámica de nuestro Departamento de Búsqueda permiten un
									servicio rápido y concreto.</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="feature wow fadeInRight" id="feature4">
							<div class="feature-icon center-block">
								<i class="fa fa-tree"></i>
							</div>
							<div class="feature-text">
								<h5 class="subtitle-margin">INVERSIÒN &amp; DESARROLLO</h5>
								<h3>ZONAS DESTACADAS
									<span class="special-color">.</span>
								</h3>
								<div class="title-separator center-block feature-separator"></div>
								<h5>La Palma</h5>
								<h5>Playa grande</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>



		<section class="featured-offers parallax" style="background-color:white;">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-offset-3 col-sm-6 text-center">
						<h5 class="subtitle-margin second-color" style="color:black;">Destacados</h5>
						<h1 class="second-color" style="color:black;">Emprendimientos
							<span class="special-color">.</span>
						</h1>
					</div>
					<div class="col-xs-12 col-sm-3 navigation-box2-cont">
						<a href="#" class="navigation-box2 navigation-box-next" id="featured-offers-owl-next">
							<div class="navigation-triangle"></div>
							<div class="navigation-box-icon2">
								<i class="jfont">&#xe802;</i>
							</div>
						</a>
						<a href="#" class="navigation-box2 navigation-box-prev" id="featured-offers-owl-prev">
							<div class="navigation-triangle"></div>
							<div class="navigation-box-icon2">
								<i class="jfont">&#xe800;</i>
							</div>
						</a>
					</div>
					<div class="col-xs-8 col-xs-offset-2 col-sm-offset-4 col-sm-4">
						<div class="title-separator-secondary2"></div>
					</div>
				</div>
			</div>
			<div class="featured-offers-container">
				<div class="owl-carousel" id="featured-offers-owl">
					<div class="featured-offer-col">
						<div class="featured-offer-front">
							<div class="featured-offer-photo">
								<img src="images/featured-offer1.jpg" alt="" />
								<div class="type-container">
									<div class="estate-type">Departamento</div>
									<div class="transaction-type">Venta</div>
								</div>
							</div>
							<div class="featured-offer-text">
								<h4 class="featured-offer-title">250 South Dr, Fort Collins, Colorado 80523, USA</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.
								</p>
							</div>
							<div class="featured-offer-params">
								<div class="featured-area">
									<img src="images/area-icon.png" alt="" />54m
									<sup>2</sup>
								</div>
								<div class="featured-rooms">
									<img src="images/rooms-icon.png" alt="" />3
								</div>
								<div class="featured-baths">
									<img src="images/bathrooms-icon.png" alt="" />1
								</div>
							</div>
							<div class="featured-price">
								$ 320 000
							</div>
						</div>
						<div class="featured-offer-back">
							<div id="featured-map1" class="featured-offer-map"></div>
							<div class="button">
								<a href="estate-details-right-sidebar.html" class="button-primary">
									<span>Leer Más</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon">
										<i class="fa fa-search"></i>
									</div>
								</a>
							</div>
						</div>

					</div>
					<div class="featured-offer-col">
						<div class="featured-offer-front">
							<div class="featured-offer-photo">
								<img src="images/featured-offer2.jpg" alt="" />
								<div class="type-container">
									<div class="estate-type">Departamento</div>
									<div class="transaction-type">venta</div>
								</div>
							</div>
							<div class="featured-offer-text">
								<h4 class="featured-offer-title">West Fourth Street, New York 10003, USA</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.
								</p>
							</div>
							<div class="featured-offer-params">
								<div class="featured-area">
									<img src="images/area-icon.png" alt="" />70m
									<sup>2</sup>
								</div>
								<div class="featured-rooms">
									<img src="images/rooms-icon.png" alt="" />4
								</div>
								<div class="featured-baths">
									<img src="images/bathrooms-icon.png" alt="" />1
								</div>
							</div>
							<div class="featured-price">
								$ 350 000
							</div>
						</div>
						<div class="featured-offer-back">
							<div id="featured-map2" class="featured-offer-map"></div>
							<div class="button">
								<a href="estate-details-right-sidebar.html" class="button-primary">
									<span>Leer más</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon">
										<i class="fa fa-search"></i>
									</div>
								</a>
							</div>
						</div>

					</div>
					<div class="featured-offer-col">
						<div class="featured-offer-front">
							<div class="featured-offer-photo">
								<img src="images/featured-offer3.jpg" alt="" />
								<div class="type-container">
									<div class="estate-type">Emprendimiento</div>
									<div class="transaction-type">Venta</div>
								</div>
							</div>
							<div class="featured-offer-text">
								<h4 class="featured-offer-title">500 E. Elwood St. Phoenix, AZ 85034, USA</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.
								</p>
							</div>
							<div class="featured-offer-params">
								<div class="featured-area">
									<img src="images/area-icon.png" alt="" />250m
									<sup>2</sup>
								</div>
								<div class="featured-rooms">
									<img src="images/rooms-icon.png" alt="" />7
								</div>
								<div class="featured-baths">
									<img src="images/bathrooms-icon.png" alt="" />3
								</div>
							</div>
							<div class="featured-price">
								$ 650 000
							</div>
						</div>
						<div class="featured-offer-back">
							<div id="featured-map3" class="featured-offer-map"></div>
							<div class="button">
								<a href="estate-details-right-sidebar.html" class="button-primary">
									<span>Leer más</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon">
										<i class="fa fa-search"></i>
									</div>
								</a>
							</div>
						</div>

					</div>
					<div class="featured-offer-col">
						<div class="featured-offer-front">
							<div class="featured-offer-photo">
								<img src="images/featured-offer4.jpg" alt="" />
								<div class="type-container">
									<div class="estate-type">Duplex</div>
									<div class="transaction-type">venta</div>
								</div>
							</div>
							<div class="featured-offer-text">
								<h4 class="featured-offer-title">N. Willamette Blvd., Portland, OR 97203-5798, USA</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.
								</p>
							</div>
							<div class="featured-offer-params">
								<div class="featured-area">
									<img src="images/area-icon.png" alt="" />40m
									<sup>2</sup>
								</div>
								<div class="featured-rooms">
									<img src="images/rooms-icon.png" alt="" />2
								</div>
								<div class="featured-baths">
									<img src="images/bathrooms-icon.png" alt="" />1
								</div>
							</div>
							<div class="featured-price">
								$ 299 000
							</div>
						</div>
						<div class="featured-offer-back">
							<div id="featured-map4" class="featured-offer-map"></div>
							<div class="button">
								<a href="estate-details-right-sidebar.html" class="button-primary">
									<span>Leer más</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon">
										<i class="fa fa-search"></i>
									</div>
								</a>
							</div>
						</div>

					</div>
					<div class="featured-offer-col">
						<div class="featured-offer-front">
							<div class="featured-offer-photo">
								<img src="images/featured-offer5.jpg" alt="" />
								<div class="type-container">
									<div class="estate-type">Emprendimiento</div>
									<div class="transaction-type">venta</div>
								</div>
							</div>
							<div class="featured-offer-text">
								<h4 class="featured-offer-title">One Brookings Drive St. Louis, Missouri 63130-4899, USA</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.
								</p>
							</div>
							<div class="featured-offer-params">
								<div class="featured-area">
									<img src="images/area-icon.png" alt="" />80m
									<sup>2</sup>
								</div>
								<div class="featured-rooms">
									<img src="images/rooms-icon.png" alt="" />3
								</div>
								<div class="featured-baths">
									<img src="images/bathrooms-icon.png" alt="" />1
								</div>
							</div>
							<div class="featured-price">
								$ 390 000
							</div>
						</div>
						<div class="featured-offer-back">
							<div id="featured-map5" class="featured-offer-map"></div>
							<div class="button">
								<a href="estate-details-right-sidebar.html" class="button-primary">
									<span>Leer Más</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon">
										<i class="fa fa-search"></i>
									</div>
								</a>
							</div>
						</div>

					</div>
					<div class="featured-offer-col">
						<div class="featured-offer-front">
							<div class="featured-offer-photo">
								<img src="images/featured-offer6.jpg" alt="" />
								<div class="type-container">
									<div class="estate-type">apartment</div>
									<div class="transaction-type">sale</div>
								</div>
							</div>
							<div class="featured-offer-text">
								<h4 class="featured-offer-title">One Neumann Drive Aston, Philadelphia 19014-1298, USA</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.
								</p>
							</div>
							<div class="featured-offer-params">
								<div class="featured-area">
									<img src="images/area-icon.png" alt="" />54m
									<sup>2</sup>
								</div>
								<div class="featured-rooms">
									<img src="images/rooms-icon.png" alt="" />3
								</div>
								<div class="featured-baths">
									<img src="images/bathrooms-icon.png" alt="" />1
								</div>
							</div>
							<div class="featured-price">
								$ 320 000
							</div>
						</div>
						<div class="featured-offer-back">
							<div id="featured-map6" class="featured-offer-map"></div>
							<div class="button">
								<a href="estate-details-right-sidebar.html" class="button-primary">
									<span>Leer más</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon">
										<i class="fa fa-search"></i>
									</div>
								</a>
							</div>
						</div>

					</div>
					<div class="featured-offer-col">
						<div class="featured-offer-front">
							<div class="featured-offer-photo">
								<img src="images/featured-offer7.jpg" alt="" />
								<div class="type-container">
									<div class="estate-type">house</div>
									<div class="transaction-type">sale</div>
								</div>
							</div>
							<div class="featured-offer-text">
								<h4 class="featured-offer-title">200 South Dr, Fort Collins, Colorado 80523, USA</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
									aliqua.
								</p>
							</div>
							<div class="featured-offer-params">
								<div class="featured-area">
									<img src="images/area-icon.png" alt="" />54m
									<sup>2</sup>
								</div>
								<div class="featured-rooms">
									<img src="images/rooms-icon.png" alt="" />3
								</div>
								<div class="featured-baths">
									<img src="images/bathrooms-icon.png" alt="" />1
								</div>
							</div>
							<div class="featured-price">
								$ 320 000
							</div>
						</div>
						<div class="featured-offer-back">
							<div id="featured-map7" class="featured-offer-map"></div>
							<div class="button">
								<a href="estate-details-right-sidebar.html" class="button-primary">
									<span>Leer más</span>
									<div class="button-triangle"></div>
									<div class="button-triangle2"></div>
									<div class="button-icon">
										<i class="fa fa-search"></i>
									</div>
								</a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>

		<section class="team section-light section-both-shadow">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-offset-3 col-sm-6 text-center">
						<h5 class="subtitle-margin">Conozcanos</h5>
						<h1>Nuestro Equipo de Profesionales
							<span class="special-color">.</span>
						</h1>
					</div>
					<div class="col-xs-12 col-sm-3 navigation-box2-cont">
						<a href="#" class="navigation-box2 navigation-box-next secondary" id="team-owl-next">
							<div class="navigation-triangle"></div>
							<div class="navigation-box-icon2">
								<i class="jfont">&#xe802;</i>
							</div>
						</a>
						<a href="#" class="navigation-box2 navigation-box-prev secondary" id="team-owl-prev">
							<div class="navigation-triangle"></div>
							<div class="navigation-box-icon2">
								<i class="jfont">&#xe800;</i>
							</div>
						</a>
					</div>
					<div class="col-xs-8 col-xs-offset-2 col-sm-offset-4 col-sm-4">
						<div class="title-separator-primary2"></div>
					</div>
				</div>
			</div>
			<div class="team-container">
				<div class="owl-carousel" id="team-owl">
					<div class="team-member-cont">
						<div class="team-member">
							<div class="team-photo">
								<img src="images/agent1.jpg" alt="" />
								<div class="big-triangle"></div>
								<div class="big-triangle2"></div>
								<a class="big-icon big-icon-plus" href="agent-right-sidebar.html">
									<i class="jfont">&#xe804;</i>
								</a>
								<div class="team-description">
									<div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-phone"></i>
											</span>
											<span>(0)-123-456-789</span>
										</div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-envelope fa-sm"></i>
											</span>
											<span>apartment@domain.tld</span>
										</div>
										<div class="team-social-cont">
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-facebook"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-twitter"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-google-plus"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-skype"></i>
												</a>
											</div>
										</div>
										<p class="team-text">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
										</p>
									</div>
								</div>
							</div>
							<div class="team-name">
								<h5>Director</h5>
								<h4>Guillermo Barrera
									<span class="special-color">.</span>
								</h4>
							</div>
						</div>
					</div>
					<div class="team-member-cont">
						<div class="team-member">
							<div class="team-photo">
								<img src="images/agent2.jpg" alt="" />
								<div class="big-triangle"></div>
								<div class="big-triangle2"></div>
								<a class="big-icon big-icon-plus" href="agent-right-sidebar.html">
									<i class="jfont">&#xe804;</i>
								</a>
								<div class="team-description">
									<div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-phone"></i>
											</span>
											<span>(0)-123-456-789</span>
										</div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-envelope fa-sm"></i>
											</span>
											<span>apartment@domain.tld</span>
										</div>
										<div class="team-social-cont">
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-facebook"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-twitter"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-google-plus"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-skype"></i>
												</a>
											</div>
										</div>
										<p class="team-text">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
										</p>
									</div>
								</div>
							</div>
							<div class="team-name">
								<h5>Agente Regional</h5>
								<h4>Nicolas Barrera
									<span class="special-color">.</span>
								</h4>
							</div>
						</div>
					</div>
					<div class="team-member-cont">
						<div class="team-member">
							<div class="team-photo">
								<img src="images/agent3.jpg" alt="" />
								<div class="big-triangle"></div>
								<div class="big-triangle2"></div>
								<a class="big-icon big-icon-plus" href="agent-right-sidebar.html">
									<i class="jfont">&#xe804;</i>
								</a>
								<div class="team-description">
									<div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-phone"></i>
											</span>
											<span>(0)-123-456-789</span>
										</div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-envelope fa-sm"></i>
											</span>
											<span>apartment@domain.tld</span>
										</div>
										<div class="team-social-cont">
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-facebook"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-twitter"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-google-plus"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-skype"></i>
												</a>
											</div>
										</div>
										<p class="team-text">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
										</p>
									</div>
								</div>
							</div>
							<div class="team-name">
								<h5>Martillero</h5>
								<h4>Mauricio Barrera
									<span class="special-color">.</span>
								</h4>
							</div>
						</div>
					</div>
					<div class="team-member-cont">
						<div class="team-member">
							<div class="team-photo">
								<img src="images/agent4.jpg" alt="" />
								<div class="big-triangle"></div>
								<div class="big-triangle2"></div>
								<a class="big-icon big-icon-plus" href="agent-right-sidebar.html">
									<i class="jfont">&#xe804;</i>
								</a>
								<div class="team-description">
									<div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-phone"></i>
											</span>
											<span>(0)-123-456-789</span>
										</div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-envelope fa-sm"></i>
											</span>
											<span>apartment@domain.tld</span>
										</div>
										<div class="team-social-cont">
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-facebook"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-twitter"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-google-plus"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-skype"></i>
												</a>
											</div>
										</div>
										<p class="team-text">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
										</p>
									</div>
								</div>
							</div>
							<div class="team-name">
								<h5>Agente</h5>
								<h4>Veronica Barrera
									<span class="special-color">.</span>
								</h4>
							</div>
						</div>
					</div>
					<div class="team-member-cont">
						<div class="team-member">
							<div class="team-photo">
								<img src="images/agent5.jpg" alt="" />
								<div class="big-triangle"></div>
								<div class="big-triangle2"></div>
								<a class="big-icon big-icon-plus" href="agent-right-sidebar.html">
									<i class="jfont">&#xe804;</i>
								</a>
								<div class="team-description">
									<div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-phone"></i>
											</span>
											<span>(0)-123-456-789</span>
										</div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-envelope fa-sm"></i>
											</span>
											<span>apartment@domain.tld</span>
										</div>
										<div class="team-social-cont">
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-facebook"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-twitter"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-google-plus"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-skype"></i>
												</a>
											</div>
										</div>
										<p class="team-text">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
										</p>
									</div>
								</div>
							</div>
							<div class="team-name">
								<h5>agent</h5>
								<h4>Timothe Lee
									<span class="special-color">.</span>
								</h4>
							</div>
						</div>
					</div>
					<div class="team-member-cont">
						<div class="team-member">
							<div class="team-photo">
								<img src="images/agent6.jpg" alt="" />
								<div class="big-triangle"></div>
								<div class="big-triangle2"></div>
								<a class="big-icon big-icon-plus" href="agent-right-sidebar.html">
									<i class="jfont">&#xe804;</i>
								</a>
								<div class="team-description">
									<div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-phone"></i>
											</span>
											<span>(0)-123-456-789</span>
										</div>
										<div class="team-desc-line">
											<span class="team-icon-circle">
												<i class="fa fa-envelope fa-sm"></i>
											</span>
											<span>apartment@domain.tld</span>
										</div>
										<div class="team-social-cont">
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-facebook"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-twitter"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-google-plus"></i>
												</a>
											</div>
											<div class="team-social">
												<a class="team-icon-circle" href="#">
													<i class="fa fa-skype"></i>
												</a>
											</div>
										</div>
										<p class="team-text">
											Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
										</p>
									</div>
								</div>
							</div>
							<div class="team-name">
								<h5>agent</h5>
								<h4>Joanne Doe
									<span class="special-color">.</span>
								</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="testimonials parallax">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-offset-3 col-sm-6 text-center">
						<h5 class="subtitle-margin second-color">Contactenos</h5>
						<h1 class="second-color">¿CONOCE EL VALOR DE SU INMUEBLE?
							<span class="special-color">.</span>
						</h1>
					</div>
					<div class="col-xs-12 col-sm-3 navigation-box2-cont">
						<a href="#" class="navigation-box2 navigation-box-next" id="testimonials-owl-next">
							<div class="navigation-triangle"></div>
							<div class="navigation-box-icon2">
								<i class="jfont">&#xe802;</i>
							</div>
						</a>
						<a href="#" class="navigation-box2 navigation-box-prev" id="testimonials-owl-prev">
							<div class="navigation-triangle"></div>
							<div class="navigation-box-icon2">
								<i class="jfont">&#xe800;</i>
							</div>
						</a>
					</div>
					<div class="col-xs-8 col-xs-offset-2 col-sm-offset-4 col-sm-4">
						<div class="title-separator-secondary2"></div>
					</div>
				</div>
			</div>

			<div class="container margin-top-90">
				<div class="row">
					<div class="col-xs-12 owl-carousel" id="testimonials-owl">
						<div class="testimonial2">

							<p style="color: #FFF; font-size: 25px; margin-left: 60px;">¿Quiere vender su inmueble o desea información gratuita sobre el valor actual de su inmueble?"</p>
						</div>


					</div>
				</div>
			</div>

		</section>

		<div id="buscador_prop">
      <div id="div_form_prop">
          <?php $search_form->deploy_search_button('do_search_button', 'BUSCAR', 'enviar_prop');?>
      </div>
      <?php $search_form->deploy_search_function('Resultado de busqueda.php');?>
</div>

		<section class="section-light no-bottom-padding section-top-shadow">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-offset-3 col-sm-6 text-center">
						<h5 class="subtitle-margin">Oportunidades</h5>
						<h1>Nuevas en el Mercado
							<span class="special-color">.</span>
						</h1>
					</div>
					<div class="col-xs-12 col-sm-3 navigation-box2-cont">
						<a href="#" class="navigation-box2 navigation-box-next secondary" id="grid-offers-owl-next">
							<div class="navigation-triangle"></div>
							<div class="navigation-box-icon2">
								<i class="jfont">&#xe802;</i>
							</div>
						</a>
						<a href="#" class="navigation-box2 navigation-box-prev secondary" id="grid-offers-owl-prev">
							<div class="navigation-triangle"></div>
							<div class="navigation-box-icon2">
								<i class="jfont">&#xe800;</i>
							</div>
						</a>
					</div>
					<div class="col-xs-8 col-xs-offset-2 col-sm-offset-4 col-sm-4">
						<div class="title-separator-primary2"></div>
					</div>
				</div>
			</div>
			<div class="grid-offers-container">
				<div class="owl-carousel" id="grid-offers-owl">
					<div class="grid-offer-col">
						<div class="grid-offer">
							<div class="grid-offer-front">
								<div class="grid-offer-photo">
									<img src="images/grid-offer1.jpg" alt="" />
									<div class="type-container">
										<div class="estate-type">Departamento</div>
										<div class="transaction-type">Venta</div>
									</div>
								</div>
								<div class="grid-offer-text">
									<i class="fa fa-map-marker grid-offer-localization"></i>
									<div class="grid-offer-h4">
										<h4 class="grid-offer-title">34 Fort Collins, Colorado 80523, USA</h4>
									</div>
									<div class="clearfix"></div>
									<p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
									<div class="clearfix"></div>
								</div>
								<div class="price-grid-cont">
									<div class="grid-price-label pull-left">Precio:</div>
									<div class="grid-price pull-right">
										$ 320 000
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="grid-offer-params">
									<div class="grid-area">
										<img src="images/area-icon.png" alt="" />54m
										<sup>2</sup>
									</div>
									<div class="grid-rooms">
										<img src="images/rooms-icon.png" alt="" />3
									</div>
									<div class="grid-baths">
										<img src="images/bathrooms-icon.png" alt="" />1
									</div>
								</div>

							</div>
							<div class="grid-offer-back">
								<div id="grid-map1" class="grid-offer-map"></div>
								<div class="button">
									<a href="estate-details-right-sidebar.html" class="button-primary">
										<span>Leer más</span>
										<div class="button-triangle"></div>
										<div class="button-triangle2"></div>
										<div class="button-icon">
											<i class="fa fa-search"></i>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="grid-offer-col">
						<div class="grid-offer">
							<div class="grid-offer-front">
								<div class="grid-offer-photo">
									<img src="images/grid-offer2.jpg" alt="" />
									<div class="type-container">
										<div class="estate-type">Departamento</div>
										<div class="transaction-type">Venta</div>
									</div>
								</div>
								<div class="grid-offer-text">
									<i class="fa fa-map-marker grid-offer-localization"></i>
									<div class="grid-offer-h4">
										<h4 class="grid-offer-title">West Fourth Street, New York 10003, USA</h4>
									</div>
									<div class="clearfix"></div>
									<p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
									<div class="clearfix"></div>
								</div>
								<div class="price-grid-cont">
									<div class="grid-price-label pull-left">Precio:</div>
									<div class="grid-price pull-right">
										$ 299 000
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="grid-offer-params">
									<div class="grid-area">
										<img src="images/area-icon.png" alt="" />48m
										<sup>2</sup>
									</div>
									<div class="grid-rooms">
										<img src="images/rooms-icon.png" alt="" />2
									</div>
									<div class="grid-baths">
										<img src="images/bathrooms-icon.png" alt="" />1
									</div>
								</div>
							</div>
							<div class="grid-offer-back">
								<div id="grid-map2" class="grid-offer-map"></div>
								<div class="button">
									<a href="estate-details-right-sidebar.html" class="button-primary">
										<span>Leer más</span>
										<div class="button-triangle"></div>
										<div class="button-triangle2"></div>
										<div class="button-icon">
											<i class="fa fa-search"></i>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="grid-offer-col">
						<div class="grid-offer">
							<div class="grid-offer-front">
								<div class="grid-offer-photo">
									<img src="images/grid-offer3.jpg" alt="" />
									<div class="type-container">
										<div class="estate-type">Departamento</div>
										<div class="transaction-type">Venta</div>
									</div>
								</div>
								<div class="grid-offer-text">
									<i class="fa fa-map-marker grid-offer-localization"></i>
									<div class="grid-offer-h4">
										<h4 class="grid-offer-title">E. Elwood St. Phoenix, AZ 85034, USA</h4>
									</div>
									<div class="clearfix"></div>
									<p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
									<div class="clearfix"></div>
								</div>
								<div class="price-grid-cont">
									<div class="grid-price-label pull-left">Precio:</div>
									<div class="grid-price pull-right">
										$ 400 000
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="grid-offer-params">
									<div class="grid-area">
										<img src="images/area-icon.png" alt="" />93m
										<sup>2</sup>
									</div>
									<div class="grid-rooms">
										<img src="images/rooms-icon.png" alt="" />4
									</div>
									<div class="grid-baths">
										<img src="images/bathrooms-icon.png" alt="" />2
									</div>
								</div>
							</div>
							<div class="grid-offer-back">
								<div id="grid-map3" class="grid-offer-map"></div>
								<div class="button">
									<a href="estate-details-right-sidebar.html" class="button-primary">
										<span>Leer más</span>
										<div class="button-triangle"></div>
										<div class="button-triangle2"></div>
										<div class="button-icon">
											<i class="fa fa-search"></i>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="grid-offer-col">
						<div class="grid-offer">
							<div class="grid-offer-front">
								<div class="grid-offer-photo">
									<img src="images/grid-offer4.jpg" alt="" />
									<div class="type-container">
										<div class="estate-type">Casa</div>
										<div class="transaction-type">venta</div>
									</div>
								</div>
								<div class="grid-offer-text">
									<i class="fa fa-map-marker grid-offer-localization"></i>
									<div class="grid-offer-h4">
										<h4 class="grid-offer-title">N. Willamette Blvd., Portland, OR 97203, USA</h4>
									</div>
									<div class="clearfix"></div>
									<p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
									<div class="clearfix"></div>
								</div>
								<div class="price-grid-cont">
									<div class="grid-price-label pull-left">Precio:</div>
									<div class="grid-price pull-right">
										$ 800 000
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="grid-offer-params">
									<div class="grid-area">
										<img src="images/area-icon.png" alt="" />300m
										<sup>2</sup>
									</div>
									<div class="grid-rooms">
										<img src="images/rooms-icon.png" alt="" />8
									</div>
									<div class="grid-baths">
										<img src="images/bathrooms-icon.png" alt="" />3
									</div>
								</div>
							</div>
							<div class="grid-offer-back">
								<div id="grid-map4" class="grid-offer-map"></div>
								<div class="button">
									<a href="estate-details-right-sidebar.html" class="button-primary">
										<span>Leer más</span>
										<div class="button-triangle"></div>
										<div class="button-triangle2"></div>
										<div class="button-icon">
											<i class="fa fa-search"></i>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="grid-offer-col">
						<div class="grid-offer">
							<div class="grid-offer-front">
								<div class="grid-offer-photo">
									<img src="images/grid-offer5.jpg" alt="" />
									<div class="type-container">
										<div class="estate-type">Departamento</div>
										<div class="transaction-type">Venta</div>
									</div>
								</div>
								<div class="grid-offer-text">
									<i class="fa fa-map-marker grid-offer-localization"></i>
									<div class="grid-offer-h4">
										<h4 class="grid-offer-title">One Brookings Drive St. Louis, Missouri 63130, USA</h4>
									</div>
									<div class="clearfix"></div>
									<p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
									<div class="clearfix"></div>
								</div>
								<div class="price-grid-cont">
									<div class="grid-price-label pull-left">Precio:</div>
									<div class="grid-price pull-right">
										$ 320 000
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="grid-offer-params">
									<div class="grid-area">
										<img src="images/area-icon.png" alt="" />50m
										<sup>2</sup>
									</div>
									<div class="grid-rooms">
										<img src="images/rooms-icon.png" alt="" />2
									</div>
									<div class="grid-baths">
										<img src="images/bathrooms-icon.png" alt="" />1
									</div>
								</div>
							</div>
							<div class="grid-offer-back">
								<div id="grid-map5" class="grid-offer-map"></div>
								<div class="button">
									<a href="estate-details-right-sidebar.html" class="button-primary">
										<span>Leer más</span>
										<div class="button-triangle"></div>
										<div class="button-triangle2"></div>
										<div class="button-icon">
											<i class="fa fa-search"></i>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="grid-offer-col">
						<div class="grid-offer">
							<div class="grid-offer-front">
								<div class="grid-offer-photo">
									<img src="images/grid-offer7.jpg" alt="" />
									<div class="type-container">
										<div class="estate-type">Casa</div>
										<div class="transaction-type">venta</div>
									</div>
								</div>
								<div class="grid-offer-text">
									<i class="fa fa-map-marker grid-offer-localization"></i>
									<div class="grid-offer-h4">
										<h4 class="grid-offer-title">One Neumann Drive Aston, Philadelphia 19014, USA</h4>
									</div>
									<div class="clearfix"></div>
									<p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et [...]</p>
									<div class="clearfix"></div>
								</div>
								<div class="price-grid-cont">
									<div class="grid-price-label pull-left">Precio:</div>
									<div class="grid-price pull-right">
										$ 500 000
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="grid-offer-params">
									<div class="grid-area">
										<img src="images/area-icon.png" alt="" />210m
										<sup>2</sup>
									</div>
									<div class="grid-rooms">
										<img src="images/rooms-icon.png" alt="" />6
									</div>
									<div class="grid-baths">
										<img src="images/bathrooms-icon.png" alt="" />2
									</div>
								</div>
							</div>
							<div class="grid-offer-back">
								<div id="grid-map6" class="grid-offer-map"></div>
								<div class="button">
									<a href="estate-details-right-sidebar.html" class="button-primary">
										<span>Leer más</span>
										<div class="button-triangle"></div>
										<div class="button-triangle2"></div>
										<div class="button-icon">
											<i class="fa fa-search"></i>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<footer class="large-cont">
			<div class="container">
				<div class="row" style="color: #000;">
					<div class="col-xs-6 col-sm-6 col-lg-3">
						<h4 class="first-color">contacto
							<span class="special-color">.</span>
						</h4>
						<div class="footer-title-separator"></div>
						<p class="footer-p">Somos Brokers especializados en el sector y aplicamos toda nuestra experiencia para llevar a cabo operaciones más seguras
							y convenientes.</p>
						<address>
							<span>
								<i class="fa fa-map-marker"></i> Matheu 163, Mar del Plata</span>
							<div class="footer-separator"></div>
							<span>
								<i class="fa fa-envelope fa-sm"></i>
								<a href="#">info@barrerapropiedades.com.ar</a>
							</span>
							<div class="footer-separator"></div>
							<span>
								<i class="fa fa-phone"></i>(0223) 4864433 / 5544</span>
						</address>
						<div class="clear"></div>
					</div>
					<div class="col-xs-6 col-sm-6 col-lg-3">
						<h4 class="first-color">Enlaces
							<span class="special-color">.</span>
						</h4>
						<div class="footer-title-separator"></div>
						<ul class="footer-ul">
							<li>
								<span class="custom-ul-bullet"></span>
								<a href="index.html">Home</a>
							</li>
							<li>
								<span class="custom-ul-bullet"></span>
								<a href="listing-grid-right-sidebar.html">Propiedades</a>
							</li>
							<li>
								<span class="custom-ul-bullet"></span>
								<a href="agencies-listing-right-sidebar.html">Emprendimiento</a>
							</li>
							<li>
								<span class="custom-ul-bullet"></span>
								<a href="archive-grid.html">Nosotros</a>
							</li>
							<li>
								<span class="custom-ul-bullet"></span>
								<a href="contact2.html">Contacto</a>
							</li>
							<li>
								<span class="custom-ul-bullet"></span>
								<a href="submit-property.html">Vender su propiedad></li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<h4 class="first-color">Barrera Propiedades
							<span class="special-color">.</span>
						</h4>
						<div class="footer-title-separator"></div>
						<div class="row">
							<div class="col-xs-6 col-sm-12">
								<article>
									<a href="blog-right-sidebar.html">
										<img src="images/footer-blog1.jpg" alt="" class="footer-blog-image" />
									</a>
									<div class="footer-blog-title">
										<a href="blog-right-sidebar.html">Título del post, lorem ipsum dolor sit</a>
									</div>
									<div class="footer-blog-date">
										<i class="fa fa-calendar-o"></i>28/09/18</div>
									<div class="clearfix"></div>
								</article>
								<div class="footer-blog-separator hidden-xs"></div>
							</div>

						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<h4 class="first-color">Tasaciones sin cargo
							<span class="special-color">.</span>
						</h4>
						<div class="footer-title-separator"></div>
						<p class="footer-p">Ingrese su número de teléfono y será atendido a la brevedad.</p>
						<form class="form-inline footer-newsletter">
							<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Teléfono">
							<button type="enviar" class="btn">
								<i class="fa fa-lg fa-paper-plane"></i>
							</button>
						</form>
					</div>
				</div>
			</div>
		</footer>
		<footer class="small-cont">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-6 small-cont">
						<img src="images/logo-dark.png" alt="" class="img-responsive footer-logo" />
					</div>
					<div class="col-xs-12 col-md-6 footer-copyrights">
						&copy; Copyright 2018
						<a href="www.treza.com.ar" target="blank">Treza</a>.
						<a href="www.treza.com.ar" target="blank">Comunicación y Diseño Inmobiliario</a>.
					</div>
				</div>
			</div>
		</footer>
	</div>

	<!-- Move to top button -->

	<div class="move-top">
		<div class="big-triangle-second-color"></div>
		<div class="big-icon-second-color">
			<i class="jfont fa-lg">&#xe803;</i>
		</div>
	</div>

	<!-- Login modal -->
	<div class="modal fade apartment-modal" id="login-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="modal-title">
						<h1>Login
							<span class="special-color">.</span>
						</h1>
						<div class="short-title-separator"></div>
					</div>
					<input name="login" type="email" class="input-full main-input" placeholder="Email" />
					<input name="password" type="password" class="input-full main-input" placeholder="Your Password" />
					<a href="my-profile.html" class="button-primary button-shadow button-full">
						<span>Sing In</span>
						<div class="button-triangle"></div>
						<div class="button-triangle2"></div>
						<div class="button-icon">
							<i class="fa fa-user"></i>
						</div>
					</a>
					<a href="#" class="forgot-link pull-right">Forgot your password?</a>
					<div class="clearfix"></div>
					<p class="login-or">OR</p>
					<a href="#" class="facebook-button">
						<i class="fa fa-facebook"></i>
						<span>Login with Facebook</span>
					</a>
					<a href="#" class="google-button margin-top-15">
						<i class="fa fa-google-plus"></i>
						<span>Login with Google</span>
					</a>
					<p class="modal-bottom">Don't have an account?
						<a href="#" class="register-link">REGISTER</a>
					</p>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<!-- Register modal -->
	<div class="modal fade apartment-modal" id="register-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="modal-title">
						<h1>Register
							<span class="special-color">.</span>
						</h1>
						<div class="short-title-separator"></div>
					</div>
					<input name="first-name" type="text" class="input-full main-input" placeholder="Frist name" />
					<input name="last-name" type="text" class="input-full main-input" placeholder="Last name" />
					<input name="email" type="email" class="input-full main-input" placeholder="Email" />
					<input name="password" type="password" class="input-full main-input" placeholder="Password" />
					<input name="repeat-password" type="password" class="input-full main-input" placeholder="Repeat Password" />
					<a href="my-profile.html" class="button-primary button-shadow button-full">
						<span>Sing up</span>
						<div class="button-triangle"></div>
						<div class="button-triangle2"></div>
						<div class="button-icon">
							<i class="fa fa-user"></i>
						</div>
					</a>
					<div class="clearfix"></div>
					<p class="login-or">OR</p>
					<a href="#" class="facebook-button">
						<i class="fa fa-facebook"></i>
						<span>Sing Up with Facebook</span>
					</a>
					<a href="#" class="google-button margin-top-15">
						<i class="fa fa-google-plus"></i>
						<span>Sing Up with Google</span>
					</a>
					<p class="modal-bottom">Already registered?
						<a href="#" class="login-link">SING IN</a>
					</p>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<!-- Forgotten password modal -->
	<div class="modal fade apartment-modal" id="forgot-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="modal-title">
						<h1>Forgot your password
							<span class="special-color">?</span>
						</h1>
						<div class="short-title-separator"></div>
					</div>
					<p class="negative-margin forgot-info">Instert your account email address.
						<br/>We will send you a link to reset your password.</p>
					<input name="login" type="email" class="input-full main-input" placeholder="Your email" />
					<a href="my-profile.html" class="button-primary button-shadow button-full">
						<span>Reset password</span>
						<div class="button-triangle"></div>
						<div class="button-triangle2"></div>
						<div class="button-icon">
							<i class="fa fa-user"></i>
						</div>
					</a>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->



	<!-- Bootstrap-->
	<script type="text/javascript" src="bootstrap/bootstrap.min.js"></script>

	<!-- Google Maps -->
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA6q80hlXqzVsaN7VXhOedzQWvvE1ZnZ84&sensor=false&libraries=places"></script>

	<!-- plugins script -->
	<script type="text/javascript" src="js/plugins.js"></script>

	<!-- template scripts -->
	<script type="text/javascript" src="mail/validate.js"></script>
	<script type="text/javascript" src="js/apartment.js"></script>

	<script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>
	<script type="text/javascript" src="js/currency-autocomplete.js"></script>

	<!-- google maps initialization -->
	<script type="text/javascript">
		google.maps.event.addDomListener(window, 'load', init);
		function init() {
			var locations = [
				[40.6128, -73.9976, "images/pin-apartment.png", "estate-details-right-sidebar.html", "images/infobox-offer1.jpg", "Fort Collins, Colorado 80523, USA", "$320 000"],
				[40.6128, -73.9976, "images/pin-apartment.png", "estate-details-right-sidebar.html", "images/infobox-offer7.jpg", "Fort Collins, Colorado 80523, USA", "$330 000"],
				[40.6128, -73.9976, "images/pin-apartment.png", "estate-details-right-sidebar.html", "images/infobox-offer3.jpg", "Fort Collins, Colorado 80523, USA", "$310 000"],
				[40.7222, -73.7903, "images/pin-commercial.png", "estate-details-right-sidebar.html", "images/infobox-offer2.jpg", "West Fourth Street, New York 10003, USA", "$350 000"],
				[41.0306, -73.7669, "images/pin-house.png", "estate-details-right-sidebar.html", "images/infobox-offer3.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$300 000"],
				[41.3006, -72.9440, "images/pin-apartment.png", "estate-details-right-sidebar.html", "images/infobox-offer4.jpg", "Fort Collins, Colorado 80523, USA", "$410 000"],
				[42.2418, -74.3626, "images/pin-land.png", "estate-details-right-sidebar.html", "images/infobox-offer5.jpg", "West Fourth Street, New York 10003, USA", "$295 000"],
				[38.8974, -77.0365, "images/pin-house.png", "estate-details-right-sidebar.html", "images/infobox-offer6.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$390 600"],
				[38.7860, -77.0129, "images/pin-apartment.png", "estate-details-right-sidebar.html", "images/infobox-offer7.jpg", "Fort Collins, Colorado 80523, USA", "$299 000"],
				[41.2693, -70.0874, "images/pin-house.png", "estate-details-right-sidebar.html", "images/infobox-offer8.jpg", "Fort Collins, Colorado 80523, USA", "$600 000"],
				[33.7544, -84.3857, "images/pin-commercial.png", "estate-details-right-sidebar.html", "images/infobox-offer9.jpg", "West Fourth Street, New York 10003, USA", "$350 000"],
				[33.7337, -84.4443, "images/pin-house.png", "estate-details-right-sidebar.html", "images/infobox-offer10.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$550 000"],
				[33.8588, -84.4858, "images/pin-land.png", "estate-details-right-sidebar.html", "images/infobox-offer1.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$670 000"],
				[34.0254, -84.3560, "images/pin-commercial.png", "estate-details-right-sidebar.html", "images/infobox-offer5.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$300 000"],
				[39.6282, -86.1320, "images/pin-apartment.png", "estate-details-right-sidebar.html", "images/infobox-offer6.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$390 600"],
				[40.5521, -84.5658, "images/pin-apartment.png", "estate-details-right-sidebar.html", "images/infobox-offer2.jpg", "West Fourth Street, New York 10003, USA", "$350 000"],
				[41.6926, -87.6021, "images/pin-house.png", "estate-details-right-sidebar.html", "images/infobox-offer5.jpg", "E. Elwood St. Phoenix, AZ 85034, USA", "$300 000"]
			];

			offersMapInit("offers-map", locations);
			mapInit(40.6128, -73.7903, "featured-map1", "images/pin-house.png", false);
			mapInit(40.7222, -73.7903, "featured-map2", "images/pin-apartment.png", false);
			mapInit(41.0306, -73.7669, "featured-map3", "images/pin-land.png", false);
			mapInit(41.3006, -72.9440, "featured-map4", "images/pin-commercial.png", false);
			mapInit(42.2418, -74.3626, "featured-map5", "images/pin-house.png", false);
			mapInit(38.8974, -77.0365, "featured-map6", "images/pin-apartment.png", false);
			mapInit(38.7860, -77.0129, "featured-map7", "images/pin-house.png", false);

			mapInit(41.2693, -70.0874, "grid-map1", "images/pin-house.png", false);
			mapInit(33.7544, -84.3857, "grid-map2", "images/pin-apartment.png", false);
			mapInit(33.7337, -84.4443, "grid-map3", "images/pin-land.png", false);
			mapInit(33.8588, -84.4858, "grid-map4", "images/pin-commercial.png", false);
			mapInit(34.0254, -84.3560, "grid-map5", "images/pin-apartment.png", false);
			mapInit(40.6128, -73.9976, "grid-map6", "images/pin-house.png", false);
		}
	</script>
	<script>	
		var localizaciones= new Array();
		$.ajax({
                    type: 'GET',
                    url: "https://tokkobroker.com/api/v1/state/?format=jsonp&lang=es_ar&limit=100&country=1&callback=jQuery171035106259967910325_1533848263827&_=1533848514011",
                    success: function (data) {
						data=data.split('(');
						data=data[1];
						data=data.split(')');
						data=data[0];
						data=JSON.parse(data);
                        console.log(data);
						for(var i=0;i<data.objects.length;i++){
							for(var j=0;j<data.objects[i].divisions.length;j++){
								console.log(data.objects[i].divisions[j]);
								localizaciones.push({ data: data.objects[i].divisions[j].id, value: data.objects[i].divisions[j].name });
							}
						}
						console.log(localizaciones);
						$('#autocomplete').autocomplete({
							lookup: localizaciones,
							onSelect: function (suggestion) {
							// some function here
							}
						});



						// setup autocomplete function pulling from currencies[] array
						$('#autocomplete').autocomplete({
							lookup: localizaciones,
							onSelect: function (suggestion) {
								
							}
						});

                    },
				});
				

				function buscarPropiedades(){
					var value= $('#autocomplete').val();
					console.log(value);
				}


	</script>
</body>

</html>